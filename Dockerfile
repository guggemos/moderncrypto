FROM python:3.7-slim

RUN apt update && apt install --yes git build-essential subversion flex bison wget subversion m4 python3 python3-dev python3-setuptools libgmp-dev libssl-dev

# install the notebook package
RUN pip install --no-cache --upgrade pip && \
    pip install --no-cache notebook  && \
    pip install --no-cache pycrypto


# create user with a home directory
ARG NB_USER
ARG NB_UID
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}

RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}
WORKDIR ${HOME}
USER ${USER}

RUN wget https://crypto.stanford.edu/pbc/files/pbc-0.5.14.tar.gz && tar xvf pbc-0.5.14.tar.gz && cd pbc-0.5.14 && ./configure LDFLAGS="-lgmp" 


USER root
WORKDIR pbc-0.5.14
RUN make && make install && ldconfig

WORKDIR ${HOME}
USER ${USER}
RUN git clone https://github.com/JHUISI/charm.git
WORKDIR charm
RUN ./configure.sh

USER root
RUN make && make install && ldconfig

RUN mkdir ${HOME}/notebooks && chown ${NB_USER} ${HOME}/notebooks

USER ${USER}
WORKDIR ${HOME}
WORKDIR notebooks
COPY --chown=${USER} notebooks .
